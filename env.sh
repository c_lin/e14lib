#######################################################
# 64 bit ROOT  
#######################################################
source /sw/packages/root/6.22.02/bin/thisroot.sh
export LD_LIBRARY_PATH=${ROOTSYS}/lib/root:${LD_LIBRARY_PATH}






#######################################################
# 64 bit Geant4/CLHEP
#######################################################
export G4INSTALL=/sw/packages/geant4/10.5.1
source $G4INSTALL/bin/geant4.sh
export G4LIB=$G4INSTALL/lib64
export G4SYSTEM=Linux-g++
export G4LIB_BUILD_SHARED=1


# e14lib
#######################################################
source /sw/koto/e14lib/201605v6.3/setup.sh
