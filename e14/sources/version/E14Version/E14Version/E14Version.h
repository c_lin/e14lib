#ifndef E14Version_h
#define E14Version_h

// char[32]  201301,201305,...
#define E14_BRANCH "201605"

// Major release corresponds to the branch. Assign manually
#define E14_RELEASE 1605

// Release version. Assign manually
#define E14_VERSION 2

// commit counter. Automatic increment. Manual reset.
#define E14_COMMIT 346

// GIT 4bit x 40 : char[40]; Assign automatically
#define E14_PREVIOUS_COMMIT "cde7c7b2ce5f685e5ccb6b015d68732785241a62"

// unsigned int 201308291939 ymdhm
#define E14_DATE 202301221407

#endif
