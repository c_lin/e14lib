#ifndef GsimVersion_h
#define GsimVersion_h

// char[32] automatic
#define GSIM_BRANCH "201605"

// Major release corresponds to the branch. Assign manually
#define GSIM_RELEASE 1605

// Release version. Assign manually
#define GSIM_VERSION 2

// commit counter. Automatic increment. Manual reset.
#define GSIM_COMMIT 266

// GIT 4bit x 40 : char[40]; Assign automatically
#define GSIM_PREVIOUS_COMMIT "cde7c7b2ce5f685e5ccb6b015d68732785241a62"

// unsigned int 201308291939 ymdhm
#define GSIM_DATE 202301221407

#endif
