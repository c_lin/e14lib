/**
 *  @file
 *  @brief  GsimE14UCV
 *  $Id$
 *  $Log$
 */
#include "GsimE14Detector/GsimE14UCV.h"
#include "GsimE14Detector/GsimE14UserGeom.h"
#include "GsimPersistency/GsimMessage.h"
#include "GsimDetector/GsimPolyhedra2.h"
#include "GsimDetector/GsimBox.h"

#include "G4SystemOfUnits.hh"

using namespace E14;

GsimE14UCV::GsimE14UCV(std::string name,
		       GsimDetector* motherDetector,
		       G4ThreeVector transV,G4ThreeVector rotV,
		       int userFlag)
  : GsimDetector(name,motherDetector,transV,rotV,userFlag)
{
  m_className = "GsimE14UCV";
  if(userFlag<202002){
    GsimMessage::getInstance()
      ->report("warning",
	       "Invalid user flag is assigned. UCV is not constructed.");
    return;
  }
  if(userFlag==202002) {
    std::cout << "UCV installed!\n";
    
    const double yLen = 140.0*mm;
    const double yedgepos = -42*mm;
    const double zPos = 0;
    //cladding is 5.8% for 1mm fiber, 4% for 0.5mm fiber including both sides
    //from Kuraray private commnuication

    double wCore = 1*(1.-5.8e-2)*mm;
    GsimBox* box = nullptr;
    for(int i=0;i<84;i++){  
      
      std::vector<double> paramVec;
      //South
      paramVec.clear();
      paramVec.push_back(wCore);
      paramVec.push_back(yLen);
      paramVec.push_back(wCore);
      
      if(box==nullptr){
    	box = new GsimBox("UCVSci",this,
			  G4ThreeVector(1.0*mm*(i-42+0.5),
					(yedgepos + yLen/2),
					zPos),
			  G4ThreeVector(0,0,0));
	
	box->setParameters(paramVec);
	box->setOuterMaterial("G4_POLYSTYRENE");
	box->setOuterColor("blue");
	box->setSensitiveDetector("UCV",i/7);
	addDaughter(box);
	m_detMoveList.push_back(box);
      }else{
	box->cloneDetector(G4ThreeVector(1.0*mm*(i-42+0.5),
					 (yedgepos + yLen/2),
					 zPos),
			   G4ThreeVector(0,0,0), i/7);
      }
    }
  } else if(userFlag>=202102){
    const float angle = -25*M_PI/180.0;
    const float cladThickness = 0.5*mm*2e-2;//2% for single side
    const float moduleGap = 0.02*mm;
    const float diaFib = 0.5*mm - 2*cladThickness;
    const int NcoreMod = 18;
    const int NeachHaloMod = 3;
    const int NfibPerMod = 16;
    const int Ncorefiber = NcoreMod * NfibPerMod;
    const int NeachHalofiber = NeachHaloMod * NfibPerMod;
    const float coreCatcherWidth = Ncorefiber*0.5*mm + (NcoreMod-1)*moduleGap;
    const float haloCatcherWidth = NeachHalofiber*0.5*mm + (NeachHaloMod-1)*moduleGap;
    GsimBox* box = nullptr;
    
    // Generates boxes for fibers in core region
    for(int i=0;i<Ncorefiber;i++){
      //Creates the fiber and sets with the position and rotation
      double loc = (-(Ncorefiber-1)*0.5/2*mm - (NcoreMod-1)*moduleGap/2 + std::floor(i/NfibPerMod)*moduleGap + i*0.5*mm);
      double y = loc*cos(angle);
      double z = loc*sin(angle);
      double x = 45.75*mm;
      int moduleID = i/(NfibPerMod*2);
      if(box==nullptr){
	box = new GsimBox("UCVSci",this,
			  G4ThreeVector(x, y, z),
			  G4ThreeVector(angle,0,0));
	
	// Sets dimensions of the first fiber
	std::vector<double> paramVec;
	paramVec.clear();
	paramVec.push_back(250*mm);
	paramVec.push_back(diaFib);
	paramVec.push_back(diaFib);
	box->setParameters(paramVec);
	box->setOuterMaterial("G4_POLYSTYRENE");
	box->setOuterColor("blue");
	box->setSensitiveDetector("UCV",moduleID);
	addDaughter(box);
	m_detMoveList.push_back(box);
	
      }else{
	box->cloneDetector(
			   G4ThreeVector(x, y, z),
			   G4ThreeVector(angle,0,0), moduleID);
      }
    }

    // Generates boxes for fibers in lower halo region
    for(int i=0;i<NeachHalofiber;i++){
      int moduleID = 9;
      if(i>=2*NfibPerMod) moduleID = 10;
      double loc = (-(NeachHalofiber-1)*0.5*mm/2 - (NeachHaloMod-1)*moduleGap/2 + std::floor(i/NfibPerMod)*moduleGap + i*0.5*mm);
      G4ThreeVector pos(0,loc,0);
      pos.rotateX(45*degree);
      pos += G4ThreeVector(-45.75*mm, -coreCatcherWidth/2, 0*mm); //moves center of halo to end of core 
      pos += G4ThreeVector(0*mm, -haloCatcherWidth/(2*sqrt(2)), -haloCatcherWidth/(2*sqrt(2))); // moves end of halo to end of core
      pos += G4ThreeVector(0*mm, -0.5*mm/2*sqrt(2) + 3/sqrt(2)*mm, 0.5*mm/2 + 3/sqrt(2)*mm); //prevent fiber in same space, add 3mm core halo overlap
      pos.rotateX(angle);
      box->cloneDetector( pos, G4ThreeVector(angle+45*M_PI/180.,0,0),moduleID);
    }
    
    
    // Generates boxes for fibers in upper halo region
    for(int i=0;i<NeachHalofiber;i++){
      int moduleID = 10;
      if(i>=NfibPerMod) moduleID = 11;
      double loc = (-(NeachHalofiber-1)*0.5*mm/2 - (NeachHaloMod-1)*moduleGap/2 + std::floor(i/NfibPerMod)*moduleGap + i*0.5*mm);
      G4ThreeVector pos(0,loc,0);
      pos.rotateX(45*degree);
      pos += G4ThreeVector(-45.75*mm, coreCatcherWidth/2, 0*mm); //moves center of halo to end of core
      pos += G4ThreeVector(0*mm, haloCatcherWidth/(2*sqrt(2)), haloCatcherWidth/(2*sqrt(2))); // moves end of halo to end of core
      pos += G4ThreeVector(0*mm, 0.5*mm/2*sqrt(2) - 3/sqrt(2)*mm, -0.5*mm/2 - 3/sqrt(2)*mm); //prevent fiber in same space, add 3mm core halo overlap
      pos.rotateX(angle);
      box->cloneDetector( pos, G4ThreeVector(angle+45*M_PI/180.,0,0),moduleID);
    }
    
    
    
  }

  setThisAndDaughterBriefName( "UCV" );
}


GsimE14UCV::~GsimE14UCV()
{
  ;
}

void GsimE14UCV::comment()
{
  if(m_userFlag==202002){
    std::cout << "GsimE14UCV Z position:" << std::endl;
    std::cout << "Plastic scintilation fiber center: z = -607 mm" << std::endl;
  } else if(m_userFlag>=202102) {
    std::cout << "GsimE14UCV Z position:" << std::endl;
    std::cout << "Plastic scintilation fiber center: z = -923.5 mm" << std::endl;
  }
}


void GsimE14UCV::setFastSimulationLevel(int level)
{
#ifdef GSIMDEBUG
  GsimMessage::getInstance()->debugEnter(__PRETTY_FUNCTION__);
#endif
  //reset
  if(m_fastSimulationLevel==6) {
    for(std::list<GsimDetector*>::iterator it=m_detMoveList.begin();
	it!=m_detMoveList.end();it++) {
      G4ThreeVector pos=(*it)->getTranslationVector();
      double z=pos.getZ();
      pos.setZ(z+29*m);
      (*it)->setTranslationVector(pos);
      (*it)->setOuterVisibility(true);
    }
  }

  //set
  if(level==6) {
    for(std::list<GsimDetector*>::iterator it=m_detMoveList.begin();
	it!=m_detMoveList.end();it++) {
      G4ThreeVector pos=(*it)->getTranslationVector();
      double z=pos.getZ();
      pos.setZ(z-29*m);
      (*it)->setTranslationVector(pos);
      (*it)->setOuterVisibility(false);
    }
  }

  
  {
    m_fastSimulationLevel=level;
    for(GsimDetectorContainer::iterator it=m_daughterDetectorContainer.begin();
	it!=m_daughterDetectorContainer.end();it++) {
      GsimDetector* daughter =  (*it).second;
      daughter->setFastSimulationLevel(level);
    }
  }
#ifdef GSIMDEBUG
  GsimMessage::getInstance()->debugExit(__PRETTY_FUNCTION__);
#endif 
}
