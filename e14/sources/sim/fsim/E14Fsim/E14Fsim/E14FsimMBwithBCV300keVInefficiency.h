#ifndef E14FsimMBwithBCV300keVInefficiency_h
#define E14FsimMBwithBCV300keVInefficiency_h
#include "E14Fsim/E14FsimInefficiency.h"
#include <list>
class TGraph;


class E14FsimMBwithBCV300keVInefficiency : public E14FsimInefficiency
{
public:
  E14FsimMBwithBCV300keVInefficiency();
  virtual ~E14FsimMBwithBCV300keVInefficiency();
};
#endif
